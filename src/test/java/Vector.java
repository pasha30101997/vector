
public class Vector {
    private double x;
    private double y;
    private double z;
    private double size=Math.pow(10,3);


    public Vector(double x, double y, double z) {

        this.x=Math.round(x*size)/size;
        this.y=Math.round(y*size)/size;
        this.z=Math.round(z*size)/size;

    }
    public double GetLength(){
        return Math.round(Math.sqrt(x*x+y*y+z*z)*size)/size;

    }
    public double ScalarProduct(Vector vector){
        return (x * vector.x + y * vector.y + z * vector.z);
    }
    public Vector VectorProduct(Vector vector) {
        return new Vector(
                y * vector.z - z * vector.y,
                z * vector.x - x * vector.z,
                x * vector.y - y * vector.x);


    }
    public double AngleBetweenVector(Vector vector){
        return  (ScalarProduct(vector) / (GetLength() * vector.GetLength()));
    }
    public Vector SumVector(Vector vector){
        return new Vector(
                x + vector.x,
                y + vector.y,
                z + vector.z
        );
    }
    public Vector SubVector(Vector vector){
        return new Vector(
                x - vector.x,
                y - vector.y,
                z - vector.z
        );
    }
    public static Vector[] generate(int n){
        Vector[] vectors = new Vector[n];
        for(int i =0; i < n; i++){
            vectors[i] = new Vector(Math.random()*10, Math.random()*10, Math.random()*10);
        }
        return vectors;
    }
    @Override
    public String toString() {
        return "{" +"x=" + x + ", y=" + y + ", z=" + z + '}';
    }


}
