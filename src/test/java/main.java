public class main {
    public static void main(String[] args) {
        Vector[] vectors = Vector.generate(6);
        System.out.println("Первый вектор: "+vectors[0]);
        System.out.println("Второй вектор: "+vectors[1]);
        System.out.println("Длина вектора: "+ vectors[0].GetLength());
        System.out.println("Скалярное произведение векторов: " + vectors[0].ScalarProduct(vectors[1]));
        System.out.println("Векторное произведение векторов: "+ vectors[0].VectorProduct(vectors[1]));
        System.out.println("Угол между векторами: " + vectors[0].AngleBetweenVector(vectors[1]));
        System.out.println("Сумма векторов: " + vectors[0].SumVector(vectors[1]));
        System.out.println("Разность векторов: " + vectors[0].SubVector(vectors[1]));



    }
}
